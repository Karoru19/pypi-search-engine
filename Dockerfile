FROM python:3

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

WORKDIR /app
RUN pip3 install poetry

RUN poetry config virtualenvs.create false
COPY poetry.lock pyproject.toml /app/
RUN poetry install

COPY . /app/