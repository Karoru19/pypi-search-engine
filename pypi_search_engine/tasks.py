from celery import shared_task
from django.core.management import call_command


@shared_task
def get_pypi_packages_info():
    call_command(
        "get_pypi_packages_info",
    )
