from pypi_search_engine.celery import app as celery_app

__all__ = ("celery_app",)
