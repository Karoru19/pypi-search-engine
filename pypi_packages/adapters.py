import feedparser
import requests
from django.conf import settings

from pypi_packages.schemas import PyPiPackageJsonSchema, PyPiPackageRssSchema


class PyPiAdapter:
    PYPI_RSS_URL = settings.PYPI_RSS_URL
    PYPI_PACKAGE_INFO_URL_PATTERN = settings.PYPI_PACKAGE_DETAILS_JSON_URL

    def __init__(self):
        self.rss_schema = PyPiPackageRssSchema(many=True)
        self.info_schema = PyPiPackageJsonSchema()

    def get_newest_packages(self) -> list[str]:
        feed = feedparser.parse(self.PYPI_RSS_URL)
        dumped_feed = self.rss_schema.dump(feed["entries"])
        return dumped_feed

    def get_package_info(self, package_name: str) -> dict:
        url = self.PYPI_PACKAGE_INFO_URL_PATTERN.format(package_name=package_name)
        response = requests.get(url)
        return self.info_schema.dump(response.json())
