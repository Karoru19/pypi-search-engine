from django.apps import AppConfig


class PypiPackagesConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "pypi_packages"
