from django.db import models


class PyPiPackage(models.Model):
    package_name = models.CharField(max_length=256)
    package_info = models.JSONField()
