from django.core.management import BaseCommand

from pypi_packages.documents import pypi_index


class Command(BaseCommand):
    def handle(self, *args, **options):
        if not pypi_index.exists():
            pypi_index.create()
