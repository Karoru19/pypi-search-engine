from urllib.parse import urlparse

from django.core.management import BaseCommand

from pypi_packages.adapters import PyPiAdapter
from pypi_packages.documents import PyPiPackageDocument
from pypi_packages.models import PyPiPackage


class Command(BaseCommand):
    adapter = PyPiAdapter()

    def handle(self, *args, **options):
        packages_names = self.__get_newest_packages_names()
        for package_name in packages_names:
            package_info = self.adapter.get_package_info(package_name)
            package, created = PyPiPackage.objects.get_or_create(
                package_name=package_name, defaults={"package_info": package_info}
            )
            if not created:
                package.package_info = package_info
                package.save()
            es_pypi_package = PyPiPackageDocument(
                meta={"id": package.pk},
                name=package_info["name"],
                description=package_info["description"],
                author=package_info.get("author", "").split(","),
                author_email=package_info.get("author_email", "").split(","),
                keywords=package_info.get("keywords", "").split(","),
                maintainer=package_info.get("maintainer", "").split(","),
                maintainer_email=package_info.get("maintainer_email", "").split(","),
                version=package_info["version"],
            )
            es_pypi_package.save()

    def __get_newest_packages_names(self) -> list[str]:
        def get_package_name(package_link: str) -> str:
            package_link_path = urlparse(package_link).path
            return package_link_path.split("/")[-2]

        return list(map(get_package_name, self.adapter.get_newest_packages()))
