from elasticsearch_dsl import Document, Index, Text, analyzer

pypi_index = Index("pypi")

pypi_index.settings(number_of_shards=1, number_of_replicas=0)


@pypi_index.document
class PyPiPackageDocument(Document):
    name = Text()
    author = Text()
    author_email = Text()
    description = Text()
    keywords = Text()
    version = Text()
    maintainer = Text()
    maintainer_email = Text()


pypi_index.document(PyPiPackageDocument)

html_strip = analyzer(
    "html_strip",
    tokenizer="pattern",
    filter=["standard", "lowercase", "stop", "snowball"],
    char_filter=["html_strip"],
)

pypi_index.analyzer(html_strip)
