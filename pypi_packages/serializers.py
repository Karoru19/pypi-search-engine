from rest_framework import serializers


class PyPiPackageDataSerializer(serializers.Serializer):
    author = serializers.CharField()
    author_email = serializers.EmailField()
    description = serializers.CharField()
    name = serializers.CharField(required=True)
    keywords = serializers.CharField()
    version = serializers.CharField()
    maintainer = serializers.CharField()
    maintainer_email = serializers.CharField()

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass


class PyPiPackageSerializer(serializers.Serializer):
    package_info = PyPiPackageDataSerializer()

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        return ret["package_info"]
