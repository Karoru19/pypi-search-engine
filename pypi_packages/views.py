from django.conf import settings
from django.views.generic import ListView
from django.views.generic.edit import FormMixin
from elasticsearch_dsl import Q
from rest_framework import generics

from pypi_packages.documents import PyPiPackageDocument
from pypi_packages.forms import PackageSearchForm
from pypi_packages.serializers import PyPiPackageDataSerializer


def query_builder(query: str) -> list:
    q = Q(
        "bool",
        must=[
            Q(
                "multi_match",
                query=query,
                fields=[
                    "name",
                    "description",
                    "author",
                    "author_name",
                    "keywords",
                    "version",
                    "maintainer",
                    "maintainer_email",
                ],
            )
        ],
    )
    s = PyPiPackageDocument.search()
    if query:
        s = s.query(q)
    return s.execute()


class PyPiPackagesListView(ListView, FormMixin):
    http_method_names = ["get", "head", "options", "trace"]
    template_name = "packages_list.html"
    paginate_by = settings.PAGINATE_BY
    form_class = PackageSearchForm

    def get_queryset(self) -> list[dict]:
        query = self.request.GET.get("query")
        return query_builder(query)

    def get_form_kwargs(self) -> dict:
        kwargs = super().get_form_kwargs()
        if self.request.method == "GET":
            data = {"query": self.request.GET.get("query", "")}
            kwargs.update({"data": data, "files": None})
        return kwargs


class PyPiPackageListAPIView(generics.ListAPIView):
    serializer_class = PyPiPackageDataSerializer

    def get_queryset(self):
        query = self.request.query_params.get("query")
        return query_builder(query)
