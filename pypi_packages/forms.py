from django import forms


class PackageSearchForm(forms.Form):
    query = forms.CharField()
