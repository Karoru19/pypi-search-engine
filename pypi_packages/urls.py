from django.urls import path

from pypi_packages import views

urlpatterns = [
    path("", views.PyPiPackagesListView.as_view(), name="packages-list"),
    path("json/", views.PyPiPackageListAPIView.as_view(), name="packages-json-list"),
]
