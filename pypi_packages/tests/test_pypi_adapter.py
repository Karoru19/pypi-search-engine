from unittest.mock import patch

import pytest

from pypi_packages.adapters import PyPiAdapter
from pypi_packages.tests.factories import JSON_TEST_OBJECT, PyPiPackageRssFactory


class TestPyPiAdapter:
    @pytest.fixture(autouse=True)
    def set_up(self):
        self.adapter = PyPiAdapter()

    @patch("pypi_packages.adapters.feedparser")
    def test_get_newest_packages(self, feedparser_mock):
        packages = PyPiPackageRssFactory.build_batch(10)
        feedparser_mock.parse.return_value = {"entries": packages}

        self.newest_packages = self.adapter.get_newest_packages()

        assert len(self.newest_packages) == 10

    @patch("pypi_packages.adapters.requests")
    def test_get_package_info(self, requests_mock):
        requests_mock.get.return_value.json.return_value = JSON_TEST_OBJECT

        package_name = "Django"
        self.package_info = self.adapter.get_package_info(package_name)

        assert self.package_info["name"] == package_name
        for field in [
            "author",
            "author_email",
            "description",
            "keywords",
            "maintainer",
            "maintainer_email",
            "version",
        ]:
            assert self.package_info[field] == JSON_TEST_OBJECT["info"][field]
