from marshmallow import Schema, fields, post_dump


class PyPiPackageRssSchema(Schema):
    link = fields.Url(required=True)

    @post_dump
    def flat_dump(self, data: dict, **kwargs) -> str:
        return data["link"]


class PyPiPackageInfoJsonSchema(Schema):
    author = fields.String()
    author_email = fields.Email()
    description = fields.String()
    name = fields.String(required=True)
    keywords = fields.String()
    version = fields.String()
    maintainer = fields.String()
    maintainer_email = fields.String()


class PyPiPackageJsonSchema(Schema):
    info = fields.Nested(PyPiPackageInfoJsonSchema())

    @post_dump
    def flat_dump(self, data: dict, **kwargs) -> dict:
        return data["info"]
