# PyPI Search Engine


## Instalacja
```
1. docker-compose build
2. docker-compose run web python manage.py migrate
3. docker-compose run web python manage.py init_index.py
```

## Uruchomienie
```commandline
docker-compose up -d
```

## Dodatkowe informacje
Pod zmienną środowiskową `PAGINATE_BY` znajdującą się w pliku `docker-compose.yml` można przestawiać liczbę elementów na liście.